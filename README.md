# TWFG contribution analytics

**status**: early development

**license**: GPLv3

**maintainer**: @eUgEntOptIc44

**development -> IDE**: [gitpod.io](https://gitpod.io) (*vscode web in docker on shared hosting*)

## workflow

1. parsing the `git log` using [`GitPython`](https://github.com/gitpython-developers/GitPython)
2. generating **svg** charts using [`pygal`](https://www.pygal.org/en/stable/)
3. generating html reports

## html reports

* [`collaborators.html`](https://tinyweatherforecastgermanygroup.gitlab.io/twfg-contribution-analytics/collaborators.html) -> charts for git collaborators
* [`working_time.html`](https://tinyweatherforecastgermanygroup.gitlab.io/twfg-contribution-analytics/working_time.html) -> working days and hours of git collaborators based on timestamps of their commit activity
* [`weblate.html`](https://tinyweatherforecastgermanygroup.gitlab.io/twfg-contribution-analytics/weblate.html) -> list of languages and translators of the [weblate](https://translate.codeberg.org/projects/tiny-weather-forecast-germany/) project
* [`pull_requests.html`](https://tinyweatherforecastgermanygroup.gitlab.io/twfg-contribution-analytics/pull_requests.html) -> list of all merge/pull requests

[`apex_charts.html`](https://tinyweatherforecastgermanygroup.gitlab.io/twfg-contribution-analytics/apex_charts.html) -> experimental github-styled (['Contributors'](https://github.com/tinyweatherforecastgermanygroup/statuspage/graphs/contributors)) interactive charts using the [apexcharts.js](https://github.com/apexcharts/apexcharts.js) JavaScript framework
