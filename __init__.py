"""
Parse git log output and saving relevant informations to json files
"""

import json
import logging
import re
import sys
from collections import OrderedDict
from pathlib import Path
from pprint import pprint

import git  # GitPython
import requests

from log_helper import logger

parent_dir = Path(__file__).parent
working_dir = parent_dir / "public"
twfg_repo = parent_dir / "TinyWeatherForecastGermany"
repo = git.Repo(twfg_repo)


def get_date_key(commit):
    return commit.committed_datetime


commits = sorted(
    list(repo.iter_commits("master")), reverse=False, key=get_date_key
)  # , max_count=5))

author_names = {}
authors_dict = OrderedDict()

weblate_langs = {}  # weblate languages

merge_req_list = []

twfg_stars = []
try:
    stars_req = requests.get(
        "https://tinyweatherforecastgermanygroup.gitlab.io/index/stargazers.json",
        timeout=100
    )  # https://codeberg.org/api/v1/repos/Starfish/TinyWeatherForecastGermany/stargazers
    if stars_req.ok:
        twfg_stars = stars_req.json()

    else:
        logger.error(
            f"while trying to fetch stargazers from gitlab pages -> error: unexpected status code '{stars_req.status_code}' "
        )
        print(str(stars_req.text))
except Exception as error_msg:
    logger.opt(exception=error_msg).error(f"failed to fetch stargazers -> error: {error_msg}")
len_twfg_stars = len(twfg_stars)

weblate_pattern = re.compile(r"(?im)[A-z ]+( using weblate \()([^)]+\)?)(\))+")

for commit_index in range(len(commits)):
    commit = commits[commit_index]
    commit_msg = str(commit.message).strip()
    # logger.debug(f" commit_msg: '{commit_msg}' ")

    commiter = commit.committer
    commiter_email = str(commiter.email).strip()
    commiter_name = str(commiter.name).strip()
    # logger.debug(f" commiter_email: '{commiter_email}' ")
    # logger.debug(f" commiter_name: '{commiter_name}' ")

    author = commit.author
    author_email = str(author.email).strip()
    author_name = str(author.name).strip()
    # logger.debug(f" author_email: '{author_email}' ")
    # logger.debug(f" author_name: '{author_name}' ")

    if author_name in author_names:
        old_author_email = str(author_names[author_name])
        if old_author_email != author_email:
            author_email = old_author_email
    else:
        author_names[author_name] = author_email

    if commiter_name.lower() in ["weblate", "codeberg translate"]:
        # logger.debug(f" weblate commit author: {author_email}")
        weblate_lang = str(
            re.sub(
                weblate_pattern,
                r"\g<2>",
                str(commit_msg.split("\n")[0]).strip(),
            )
        ).strip()
        logger.debug(f"weblate_lang -> {weblate_lang}")
        if weblate_lang not in weblate_langs:
            weblate_langs[weblate_lang] = [author_name]
            # TODO: count lines for sorting?
        else:
            if author_name not in weblate_langs[weblate_lang]:
                weblate_langs[weblate_lang].append(author_name)

    commit_date = commit.committed_datetime.strftime("%d.%m.%Y")

    commit_stats = commit.stats.total
    del commit_stats["files"]
    commit_stats["commits"] = 1
    # if commit_stats['deletions'] > 0:
    #    commit_stats['deletions'] * -1

    try:
        # api endpoint: https://codeberg.org/api/v1/repos/Starfish/TinyWeatherForecastGermany/pulls
        if "merge pull request" in str(commit_msg).lower():
            pull_commit = commits[commit_index - 1]
            merge_req_list.append(
                {
                    "date": int(commit.committed_datetime.timestamp()),
                    "message": commit_msg,
                    "commit": {
                        "author": {"name": author_name, "email": author_email},
                        "commiter": {"name": commiter_name, "email": commiter_email},
                    },
                    "pull request": {
                        "author": {
                            "name": str(pull_commit.author.name).strip(),
                            "email": str(pull_commit.author.email),
                        },
                        "commiter": {
                            "name": str(pull_commit.committer.name),
                            "email": str(pull_commit.committer.email),
                        },
                    },
                }
            )
    except Exception as error_msg:
        logger.opt(exception=error_msg).error(
            f"failed to parse commit message for merge requests -> error: {error_msg}"
        )

    if author_email not in authors_dict:
        user_name = str(author_name)
        if len_twfg_stars > 0:
            found_login = False
            for star_user in twfg_stars:
                if "codeberg" in str(star_user["html_url"]):
                    if (
                        str(star_user["login"]) == user_name
                        or str(star_user["full_name"]) == user_name
                    ):
                        user_name = str(star_user["login"])
                        found_login = True
                        logger.debug(
                            f"found user '{user_name}' -> '{star_user['login']}' in stargazer list"
                        )
                        break

            if found_login is False:
                for star_user in twfg_stars:
                    if (
                        str(star_user["login"]) == user_name
                        or str(star_user["full_name"]) == user_name
                    ):
                        user_name = str(star_user["login"])
                        found_login = True
                        logger.debug(
                            f"found user '{user_name}' -> '{star_user['login']}' in stargazer list"
                        )
                        break

        users_api = "https://codeberg.org/api/v1/users/"

        user_dict = requests.get(users_api + author_name, timeout=100).json()

        found_user = False
        if "login" not in user_dict:
            try:
                commit_sha = str(commit.hexsha)
                commit_req = requests.get(
                    f"https://codeberg.org/api/v1/repos/Starfish/TinyWeatherForecastGermany/commits?sha={commit_sha}&limit=1",
                    timeout=100
                )
                if commit_req.ok:
                    commit_json = commit_req.json()
                    if len(commit_json) > 0:
                        commit_json = commit_json[0]
                        if "author" in commit_json:
                            author_json = commit_json["author"]
                            if author_json != None:
                                user_dict = author_json
                                found_user = True

            except Exception as error_msg:
                logger.opt(exception=error_msg).error(f"failed to fetch commit for user"
                                                      f" -> error: {error_msg}")

            # if found_user != True:
            #     user_email_name = str(author_email.split('@')[0]).strip()
            #     if user_email_name.lower() != 'git':
            #         users_api = "https://codeberg.org/api/v1/users/search"
            #         users_params = {'q':str(user_email_name)}
            #         user_dict = requests.get(users_api, params=users_params).json()
            #         if len(user_dict['data']) > 0:
            #             found_user = True
            #             user_dict = user_dict['data'][0]
        else:
            found_user = True

        authors_dict[author_email] = {
            "name": author_name,
            "activity": {commit_date: commit_stats},
            "timestamps": [int(commit.committed_datetime.timestamp())],
        }
        if found_user:
            authors_dict[author_email]["user"] = user_dict
    else:
        author_activity = authors_dict[author_email]["activity"]
        if commit_date not in author_activity:
            author_activity[commit_date] = commit_stats
        else:
            author_activity[commit_date]["deletions"] += commit_stats["deletions"]
            author_activity[commit_date]["insertions"] += commit_stats["insertions"]
            author_activity[commit_date]["lines"] += commit_stats["lines"]
            author_activity[commit_date]["commits"] += 1
        authors_dict[author_email]["timestamps"].append(
            int(commit.committed_datetime.timestamp())
        )

try:
    # pprint(authors_dict.items())
    authors_dict = OrderedDict(
        sorted(
            authors_dict.items(),
            key=lambda item: len(list(item[1]["activity"].keys())),
            reverse=True,
        )
    )
except Exception as error_msg:
    logger.opt(exception=error_msg).error(f"failed to sort 'authors_dict' -> error: {error_msg}")
# pprint(authors_dict)

with open(working_dir / "repo_collaborators.json", "w", encoding="utf-8") as fh:
    fh.write(str(json.dumps(authors_dict, indent=4, sort_keys=False)))

with open(working_dir / "weblate_contributors.json", "w", encoding="utf-8") as fh:
    fh.write(str(json.dumps(weblate_langs, indent=4, sort_keys=True)))

with open(working_dir / "merge_requests.json", "w", encoding="utf-8") as fh:
    fh.write(str(json.dumps(merge_req_list, indent=4, sort_keys=True)))

with open(working_dir / "stargazers.json", "w", encoding="utf-8") as fh:
    fh.write(str(json.dumps(twfg_stars, indent=4, sort_keys=True)))
