import sys
import logging
from pathlib import Path

import loguru

from logging_interceptor import InterceptHandler

parent_dir = Path(__file__).parent

logger = loguru.logger
logger.remove()
# init rotated log file
logger.add(parent_dir / "public" / "debug.log", rotation="10 MB", retention="7 days",
           backtrace=True, delay=True, enqueue=True, diagnose=True)
# also log to standard output (console)
logger.add(sys.stdout, colorize=True)

# activate logging
logging.basicConfig(handlers=[InterceptHandler()], level=logging.DEBUG, force=True)