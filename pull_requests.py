import json
import re
import sys
from pathlib import Path
from pprint import pprint

import htmlmin
import markdown
import requests
from markdown.extensions.codehilite import CodeHiliteExtension
from markdown.extensions.toc import TocExtension

from log_helper import logger


headers = {"User-Agent": "", "DNT": "1"}

pulls_json_path = Path("pull_requests.json")
pulls_json = []
pulls_md_path = Path("pull_requests.md")
pulls_md = ""
pulls_html_path = Path("pull_requests.html")
pulls_html = ""

if pulls_json_path.exists():
    with open(pulls_json_path, "r", encoding="utf-8") as fh:
        pulls_json = json.loads(str(fh.read()))
else:
    pulls_params = {"page": 1}
    next_page = True

    while next_page:
        pulls_req = requests.get(
            "https://codeberg.org/api/v1/repos/Starfish/TinyWeatherForecastGermany/pulls",
            params=pulls_params,
            headers=headers,
            timeout=100,
        )

        if pulls_req.ok:
            json_temp = pulls_req.json()
            if len(json_temp) == 0:
                next_page = False
            else:
                pulls_json += json_temp
                pulls_params["page"] += 1
        else:
            next_page = False

    with open(pulls_json_path, "w+", encoding="utf-8") as fh:
        json.dump(pulls_json, fh, indent=4)

pulls_md += f"# {len(pulls_json)} merge/pull requests\n"

for pr_dict in pulls_json:
    try:
        logger.debug(f"processing pull request '{pr_dict['title']}' ...")
        pulls_md += (
            f"\n## #{pr_dict['number']} - [{pr_dict['title']}]({pr_dict['html_url']})\n"
        )

        if pr_dict["base"]["repo"] is not None:
            pulls_md += f"\n[`Starfish:{pr_dict['base']['label']}`]({pr_dict['base']['repo']['html_url']}/src/branch/{pr_dict['base']['label']})"
        else:
            pulls_md += f"\n`Starfish:{pr_dict['base']['label']}`"

        if pr_dict["head"]["repo"] is not None:
            pulls_md += f" <-- [`{pr_dict['user']['login']}:{pr_dict['head']['label']}`]({pr_dict['head']['repo']['html_url']}/src/branch/{pr_dict['head']['label']})\n"
        else:
            pulls_md += (
                f" <-- `{pr_dict['user']['login']}:{pr_dict['head']['label']}`\n"
            )

        pulls_md += (
            f"\n{pr_dict['updated_at']} - ![avatar image of @{pr_dict['user']['login']}]({pr_dict['user']['avatar_url']})"
            + "{ width=25px loading=lazy decoding=async }"
            + f"@{pr_dict['user']['login']}"
        )

        if len(str(pr_dict["user"]["full_name"]).replace("None", "").strip()) > 1:
            pulls_md += f" ({pr_dict['user']['full_name']})\n"
        else:
            pulls_md += "\n"

        pr_body = str(pr_dict["body"]).strip()

        # make footnotes unique
        pr_body = re.sub(
            r"(\[\^)([\d]+)(\])",
            r"\g<1>" + str(pr_dict["number"]) + r"_\g<2>\g<3>",
            pr_body,
        )

        # lazy load images
        pr_body = re.sub(
            r"(\!\[[^\]]*\]\([^\)]+\))([^\{])",
            r"\g<1>{ loading=lazy decoding=async }\g<2>",
            pr_body,
        )

        pulls_md += f"\n{pr_body}\n"
    except Exception as error_msg:
        logger.opt(exception=error_msg).error(
            f"while processing pull request '{pr_dict['title']}' -> error: {error_msg}"
        )


with open(pulls_md_path, "w+", encoding="utf-8") as fh:
    fh.write(str(pulls_md))

pulls_md = "\n\n[TOC]\n\n" + pulls_md

pulls_html = '<!DOCTYPE html><html lang="en"><head>'

pulls_html += """
<meta charset="utf-8">
<title>TinyWeatherForecastGermany - merge/pull requests</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" sizes="180x180" href="../index/images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="../index/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="../index/images/favicon-16x16.png">
<link rel="manifest" href="../index/images/site.webmanifest">
<link rel="mask-icon" href="../index/images/safari-pinned-tab.svg" color="#293235">
<link rel="shortcut icon" href="../index/images/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Tiny Weather Forecast Germany">
<meta name="application-name" content="Tiny Weather Forecast Germany">
<meta name="msapplication-TileColor" content="#293235">
<meta name="msapplication-config" content="../index/images/browserconfig.xml">
<meta name="theme-color" content="#293235">
<meta name="robots" content="noindex, nofollow">
<meta name="thumbnail" content="../index/images/icon.png">
</head>
"""

css_style = """
<style>
:root {
    scrollbar-color: rgb(210, 210, 210) rgb(46, 54, 69) !important;
    scrollbar-width: thin !important;
}

body::-webkit-scrollbar {
    width: 7px;
    height: 15px
}

body::-webkit-scrollbar-track {
    background-color: rgb(210, 210, 210) !important;
}

body::-webkit-scrollbar-thumb {
    background-color: rgb(46, 54, 69) !important;
    height: 30px
}

#markdown-content {
    margin: 10px auto;
    max-width: 1200px;
}

img {
    max-width: 98%;
}

img.emojione {
    width: 20px;
}

div.toc li a,
h4 a,
p a,
a.toclink,
a.footnote-ref,
a.footnote-backref {
    text-decoration:none;
}
div.toc li a:hover,
h4 a:hover,
p a:hover,
a.toclink:hover,
a.footnote-ref:hover,
a.footnote-backref:hover {
    text-decoration:underline;
}

div.toc ul {
    counter-reset: section;
    list-style-type: none;
    padding-left: 15px;
}
div.toc ul li::before {
    counter-increment: section; 
    content: counters(section,".") " ";
}

.task-list-item {
  list-style-type: none !important;
}

.task-list-item input[type="checkbox"] {
  margin: 0 4px 0.25em -20px;
  vertical-align: middle;
}

.task-list-control {
  display: inline; /* Ensure label is inline incase theme sets it to block.*/
}

.task-list-control {
  position: relative;
  display: inline-block;
  color: #555;
  cursor: pointer;
}

.task-list-control input[type="checkbox"] {
  position: absolute;
  opacity: 0;
  z-index: -1; /* Put the input behind the label so it doesn't overlay text */
}

.task-list-indicator {
  position: absolute;
  top: -8px;
  left: -18px;
  display: block;
  width:  14px;
  height: 14px;
  color: #eee;
  background-color: #eee;
  border-radius: .25rem;
}

.task-list-control input[type="checkbox"]:checked + .task-list-indicator::before {
  display: block;
  margin-top: -4px;
  margin-left: 2px;
  font-size: 16px;
  line-height: 1;
  content: "✔";
  color: #1EBB52;
}


@media (prefers-color-scheme: dark) {
    *,
    body {
        background-color: #000000 !important;
        color: #ccc;
    }

    div,
    p,
    ul,
    li,
    strong,
    b,
    progress,
    pre {
        background-color: transparent !important;
        color: #CCCCCC !important;
    }
    code span {
        filter: invert(1) !important;
        background-color: transparent !important;
    }
}
@media print {
    *,
    body {
        background-color: transparent !important;
        color: #000000 !important;
    }
    div.toc {
        page-break-after:always;
    }
    p a[href]::after {
        content: " ("attr(href)")";
        color: #000000;
        font-style: italic;
        word-break: break-word;
    }
    a.toclink::after,
    p a.footnote-ref::after,
    p a.footnote-backref::after {
        content: "" !important;
    }
    p a.footnote-backref {
        display:none;
    }
}
</style>
"""

css_style = re.sub(r"(?m)[\r\n\t]+", "", css_style)

pulls_html += css_style + '</head><body><div id="markdown-content">'

pulls_html += markdown.markdown(
    pulls_md,
    extensions=[
        "pymdownx.magiclink",
        "pymdownx.betterem",
        "pymdownx.caret",
        "pymdownx.critic",
        "pymdownx.details",
        "pymdownx.emoji",
        "pymdownx.escapeall",
        "pymdownx.highlight",
        "pymdownx.keys",
        "pymdownx.mark",
        "pymdownx.smartsymbols",
        "pymdownx.tasklist",
        "pymdownx.tilde",
        "extra",
        "sane_lists",
        CodeHiliteExtension(noclasses=True),
        TocExtension(baselevel=3, title="Table of contents", anchorlink=True),
    ],
)

pulls_html += "</div></body></html>"

try:
    with open(pulls_html_path, "w+", encoding="utf-8") as fh:
        fh.write(
            htmlmin.minify(
                str(pulls_html), remove_empty_space=True, remove_comments=True
            )
        )
except Exception as error_msg:
    logger.opt(exception=error_msg).error(f"minification of '{pulls_html_path}' failed -> error: {error_msg}")
    with open(pulls_html_path, "w+", encoding="utf-8") as fh:
        fh.write(str(pulls_html))
