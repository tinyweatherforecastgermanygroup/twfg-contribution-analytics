import json
import re
from datetime import datetime

import pygal
from pygal.style import Style

with open("repo_collaborators.json", "r", encoding="utf-8") as fh:
    all_collaborators = json.loads(str(fh.read()))

html_str = '<!DOCTYPE html><html lang="en"><head>'

html_str += """
<meta charset="utf-8">
<title>TinyWeatherForecastGermany - git Collaborators - Insertions and Deletions</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" sizes="180x180" href="../index/images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="../index/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="../index/images/favicon-16x16.png">
<link rel="manifest" href="../index/images/site.webmanifest">
<link rel="mask-icon" href="../index/images/safari-pinned-tab.svg" color="#293235">
<link rel="shortcut icon" href="../index/images/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Tiny Weather Forecast Germany">
<meta name="application-name" content="Tiny Weather Forecast Germany">
<meta name="msapplication-TileColor" content="#293235">
<meta name="msapplication-config" content="../index/images/browserconfig.xml">
<meta name="theme-color" content="#293235">
<meta name="robots" content="noindex, nofollow">
<meta name="thumbnail" content="../index/images/icon.png">
"""

html_str += '<style>:root {scrollbar-color: rgb(210,210,210) rgb(46,54,69) !important;scrollbar-width: thin !important;} p.contributor-meta-line {padding:3px;} p.contributor-meta-line:odd {border-bottom:1px dashed #000000;} @media (prefers-color-scheme: dark) {*, body {background-color:#000000!important; color: #ccc;} div, p, object, ul, li, p.contributor-meta-line {background-color:transparent!important; color: #ccc!important;}.user-lines-chart,.user-commits-chart{filter:invert(1);}}</style></head><body><div id="main-container" style="max-width: 80%; margin-right:auto; margin-left:auto;margin-top:20px;margin-bottom:20px;">'

html_str += (
    "<h1>TinyWeatherForecastGermany - git Collaborators</h1><p>Insertions and deletions of "
    + str(len(all_collaborators.keys()))
    + " Collaborators.</p><hr>"
)

for collaborator_email in all_collaborators.keys():
    user_temp = all_collaborators[str(collaborator_email)]
    user_activity = user_temp["activity"]

    user_days = []
    user_insertions = []
    user_deletions = []
    user_lines = []
    user_commits = []

    if len(user_activity.keys()) < 2:
        html_str += '<div style="width: 700px; max-width: 98%; min-width: 30%; float:left;"><p class="contributor-meta-line">'

        if "user" in user_temp and "avatar_url" in user_temp["user"]:
            avatar_url = str(user_temp["user"]["avatar_url"])
            html_str += (
                '<img src="'
                + avatar_url
                + '" width="25px" height="25px" loading="lazy">&nbsp;'
            )

        html_str += (
            str(user_temp["name"]) + " (" + str(collaborator_email) + ")" + "</p><ul>"
        )

        for day_date in user_activity.keys():
            day_dict = user_activity[day_date]
            html_str += (
                "<li>"
                + str(day_date)
                + " &nbsp;&rarr; "
                + str(day_dict["insertions"])
                + " insertion(s), "
                + str(day_dict["deletions"])
                + " deletion(s), "
                + str(day_dict["commits"])
                + " commit(s), "
                + str(day_dict["lines"])
                + " lines(s) "
                + "</li>"
            )

        html_str += "</ul></div>"
    else:
        for day_date in user_activity.keys():
            # user_days.append(datetime.fromtimestamp(int(day_date)).strftime('%d.%m.%Y'))
            day_dict = user_activity[day_date]
            user_insertions.append(day_dict["insertions"])
            user_deletions.append(day_dict["deletions"] * -1)
            user_lines.append(day_dict["lines"])
            user_commits.append(day_dict["commits"])

        chart_style = Style(
            background="rgba(255, 255, 255, 0)",
            plot_background="rgba(255, 255, 255, 0.9)",
            foreground="rgba(0, 0, 0, 0.8)",
            foreground_light="rgba(0, 0, 0, 0.9)",
            foreground_dark="rgba(0, 0, 0, 0.7)",
            colors=(
                "#9DC700",
                "#C70039",
                "#60BD68",
                "#F17CB0",
                "#4D4D4D",
                "#B2912F",
                "#B276B2",
                "#DECF3F",
                "#F15854",
            ),  # ('#5DA5DA', '#FAA43A','#60BD68', '#F17CB0', '#4D4D4D', '#B2912F','#B276B2', '#DECF3F', '#F15854')
        )

        line_chart = pygal.Line(
            fill=True,
            show_legend=False,
            human_readable=True,
            interpolate="cubic",
            style=chart_style,
            show_dots=True,
            show_x_guides=False,
            show_y_guides=False,
            js=["pygal-tooltips.min.js"],
        )
        line_chart.title = ""
        line_chart.x_labels = (
            user_activity.keys()
        )  # sorted(user_activity.keys(), reverse=True)
        if len(user_activity.keys()) > 20:
            line_chart.show_x_labels = False

        line_chart.add("insertions", user_insertions)
        line_chart.add("deletions", user_deletions)

        chart_style2 = Style(
            background="rgba(255, 255, 255, 0)",
            plot_background="rgba(255, 255, 255, 0.9)",
            foreground="rgba(0, 0, 0, 0.8)",
            foreground_light="rgba(0, 0, 0, 0.9)",
            foreground_dark="rgba(0, 0, 0, 0.7)",
            colors=(
                "#404040",
                "#C70039",
                "#60BD68",
                "#F17CB0",
                "#4D4D4D",
                "#B2912F",
                "#B276B2",
                "#DECF3F",
                "#F15854",
            ),
        )

        commits_chart = pygal.Bar(style=chart_style2)
        commits_chart.add("commits", user_commits)
        sparkline_svg = str(commits_chart.render_sparkline().decode("utf-8"))
        sparkline_svg = str(re.sub(r"(?m)(<\!\-\-)(.)+(\-\->)", "", str(sparkline_svg)))
        sparkline_svg = str(re.sub(r"(?m)[\t\r\n]+", "", str(sparkline_svg)))

        svg_temp = str(
            line_chart.render().decode("utf-8")
        )  # disable_xml_declaration=True
        svg_temp = str(re.sub(r"(?m)(<\!\-\-)(.)+(\-\->)", "", str(svg_temp)))
        svg_temp = str(re.sub(r"(?m)[\t\r\n]+", "", str(svg_temp)))
        svg_file_name = (
            "lines_" + re.sub(r"(?im)[^A-z0-9]+", "", str(user_temp["name"])) + ".svg"
        )

        with open(svg_file_name, "w", encoding="utf-8") as fh:
            fh.write(svg_temp)

        with open(
            svg_file_name.replace("lines_", "spark_"), "w", encoding="utf-8"
        ) as fh:
            fh.write(sparkline_svg)

        html_str += '<div style="width: 700px; max-width: 98%; min-width: 30%; float:left; background-color:transparent;"><p class="contributor-meta-line">'

        if "user" in user_temp and "avatar_url" in user_temp["user"]:
            avatar_url = str(user_temp["user"]["avatar_url"])
            html_str += (
                '<img src="'
                + avatar_url
                + '" width="25px" height="25px" loading="lazy">&nbsp;'
            )

        svg_tags = (
            '<object class="user-lines-chart" style="" data="'
            + str(svg_file_name)
            + '" type="image/svg+xml"></object>'
        )

        html_str += (
            str(user_temp["name"])
            + " ("
            + str(collaborator_email)
            + ")"
            + '<object class="user-commits-chart" title="commits by '
            + str(user_temp["name"])
            + '" style="height:20px;background:transparent;" data="'
            + str(svg_file_name.replace("lines_", "spark_"))
            + '" type="image/svg+xml"></object></p>'
            + svg_tags
            + "</div>"
        )

    # break

html_str += (
    '<div style="width:98%;clear:both"><p>Last Modified: '
    + str(datetime.now().strftime("%Y-%m-%d %H:%M:%S (UTC)"))
    + "</p></div>"
)

html_str += "</div></body></html>"
with open("collaborators.html", "w", encoding="utf-8") as fh:
    fh.write(html_str)
