"""
Create svg charts using pygal for working days and hours retrieved from git log

Why? -> To know when a contributor is most likely active and able to respond to requests.

Status: WIP (!)

"""

import json
import re
import sys
from datetime import datetime
from pprint import pprint
from pathlib import Path

import pygal
from dateutil import parser as dtuparser
from dateutil.tz import (  # timezone UTC -> docs: https://dateutil.readthedocs.io/en/stable/tz.html#dateutil.tz.tzutc
    UTC,
    tzstr,
    tzutc,
)
from pygal.style import Style

from log_helper import logger


parent_dir = Path(__file__).parent
public_dir = parent_dir / "public"

with open(public_dir / "repo_collaborators.json", "r", encoding="utf-8") as fh:
    all_collaborators = json.loads(str(fh.read()))

html_str = '<!DOCTYPE html><html lang="en"><head>'

html_str += """
<meta charset="utf-8">
<title>TinyWeatherForecastGermany - git Collaborators - Working days and hours</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" sizes="180x180" href="../index/images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="../index/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="../index/images/favicon-16x16.png">
<link rel="manifest" href="../index/images/site.webmanifest">
<link rel="mask-icon" href="../index/images/safari-pinned-tab.svg" color="#293235">
<link rel="shortcut icon" href="../index/images/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Tiny Weather Forecast Germany">
<meta name="application-name" content="Tiny Weather Forecast Germany">
<meta name="msapplication-TileColor" content="#293235">
<meta name="msapplication-config" content="../index/images/browserconfig.xml">
<meta name="theme-color" content="#293235">
<meta name="robots" content="noindex, nofollow">
<meta name="thumbnail" content="../index/images/icon.png">
"""

html_str += '<style>:root {scrollbar-color: rgb(210,210,210) rgb(46,54,69) !important;scrollbar-width: thin !important;} p.contributor-meta-line {padding:3px;} p.contributor-meta-line:odd {border-bottom:1px dashed #000000;} @media (prefers-color-scheme: dark) {*, body {background-color:#000000!important; color: #ccc;} div, p, object, ul, li, p.contributor-meta-line {background-color:transparent!important; color: #ccc!important;}.user-days-chart,.user-hours-chart{filter:invert(1);}}</style></head><body><div id="main-container" style="max-width: 80%; margin-right:auto; margin-left:auto;margin-top:20px;margin-bottom:20px;">'

html_str += (
    "<h1>TinyWeatherForecastGermany - git Collaborators</h1><p>Working days and hours of "
    + str(len(all_collaborators.keys()))
    + " Collaborators.</p><hr>"
)

html_str += '<div id="tz-info-box"><p>Timezone of all timestamps → UTC</p></div>'
html_str += '<script>var d = new Date()\nvar gmtHours = -d.getTimezoneOffset()/60;\nif(gmtHours > 0){gmtHours = "+"+gmtHours;}\nvar tz_str = document.createElement("span");\ntz_str.innerHTML = "<br>Local timzone: "+gmtHours+" hour(s) difference to UTC";\ndocument.querySelector("#tz-info-box p").append(tz_str);</script>'  # also see: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/getTimezoneOffset

for collaborator_email in all_collaborators.keys():
    try:
        user_temp = all_collaborators[str(collaborator_email)]

        user_timestamps = user_temp["timestamps"]

        if len(user_timestamps) < 2:
            html_str += '<div style="width: 700px; max-width: 98%; min-width: 30%; float:left;"><p class="contributor-meta-line">'

            if "user" in user_temp and "avatar_url" in user_temp["user"]:
                avatar_url = str(user_temp["user"]["avatar_url"])
                html_str += (
                    f'<img src="{avatar_url}" width="25px" height="25px" loading="lazy">&nbsp;'
                )

            html_str += (
                f"{user_temp['name']} ({collaborator_email})</p><ul>"
            )

            for unix_time in user_timestamps:
                #    day_dict = user_activity[day_date]
                #    html_str += '<li>' + str(day_date) + ' &nbsp;&rarr; ' + str(day_dict['insertions']) + ' insertion(s), '  + str(day_dict['deletions']) + ' deletion(s), '  + str(day_dict['commits']) + ' commit(s), '  + str(day_dict['lines']) + ' lines(s) ' + '</li>'
                html_str += (
                    "<li>"
                    + str(
                        datetime.fromtimestamp(unix_time).strftime(
                            "%A, %d. %B %Y at %H:%M:%S (UTC)"
                        )
                    )
                    + "</li>"
                )

            html_str += "</ul></div>"
        else:
            # pprint(user_timestamps)
            user_hours = [0 for x in range(1, 25)]
            user_days = [0 for x in range(0, 7)]
            for unix_time in user_timestamps:
                unix_time_utc = datetime.fromtimestamp(unix_time).astimezone(UTC)
                user_hours[
                    int(
                        unix_time_utc.strftime("%H")
                    )
                    - 1
                ] += 1
                day_int = int(
                    unix_time_utc.strftime("%w")
                )
                if day_int == 0:
                    day_int = 6
                else:
                    day_int -= 1
                user_days[day_int] += 1

            html_str += '<div style="width: 700px; max-width: 98%; min-width: 30%; float:left;"><p class="contributor-meta-line">'

            if "user" in user_temp and "avatar_url" in user_temp["user"]:
                avatar_url = str(user_temp["user"]["avatar_url"])
                html_str += (
                    f'<img src="{avatar_url}" width="25px" height="25px" loading="lazy">&nbsp;'
                )

            html_str += (
                f"{user_temp['name']} ({collaborator_email})</p>"
            )

            chart_style = Style(
                background="rgba(255, 255, 255, 0)",
                plot_background="rgba(255, 255, 255, 0.9)",
                foreground="rgba(0, 0, 0, 0.8)",
                foreground_light="rgba(0, 0, 0, 0.9)",
                foreground_dark="rgba(0, 0, 0, 0.7)",
                colors=(
                    "#DECF3F",
                    "#B2912F",
                    "#9DC700",
                    "#C70039",
                    "#60BD68",
                    "#F17CB0",
                    "#4D4D4D",
                    "#B276B2",
                    "#F15854",
                ),
            )

            svg_file_name = (
                re.sub(r"(?im)[^A-z\d]+", "", str(user_temp["name"])) + ".svg"
            )

            hours_chart = pygal.Line(
                fill=True,
                show_legend=False,
                human_readable=True,
                interpolate=None,
                style=chart_style,
                js=["pygal-tooltips.min.js"],
            )
            hours_chart.title = ""
            hours_chart.x_labels = [
                int(x) for x in range(1, 25)
            ]  # labels -> hours 1 to 24
            hours_chart.add("commits", user_hours)
            hours_chart.x_value_formatter = (
                lambda x: "%sh (UTC)" % x
            )  # add 'h' to labels of x axis

            hours_svg = str(hours_chart.render().decode("utf-8"))

            hours_svg = str(re.sub(r"(?m)(<\!\-\-)(.)+(\-\->)", "", str(hours_svg)))
            svg_file_name = "hours_" + svg_file_name

            with open(public_dir / svg_file_name, "w", encoding="utf-8") as fh:
                fh.write(hours_svg)

            html_str += "<strong>Hours</strong>"
            html_str += (
                f'<object class="user-hours-chart" style="" data="{svg_file_name}'
                f'" type="image/svg+xml"></object>'
            )

            days_chart = pygal.Bar(
                fill=True,
                show_legend=False,
                human_readable=True,
                interpolate=None,
                style=chart_style,
                js=["pygal-tooltips.min.js"],
            )
            days_chart.title = ""
            days_chart.x_labels = [
                int(x) for x in range(0, 7)
            ]  # labels -> indices of week_days
            days_chart.add("commits", user_days)
            week_days = [
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday",
                "Sunday",
            ]
            days_chart.x_value_formatter = lambda x: week_days[x]  #

            days_svg = str(days_chart.render().decode("utf-8"))

            days_svg = str(re.sub(r"(?m)(<\!\-\-)(.)+(\-\->)", "", str(days_svg)))
            svg_file_name = "days_" + svg_file_name

            with open(public_dir / svg_file_name, "w", encoding="utf-8") as fh:
                fh.write(days_svg)

            html_str += "<strong>Days</strong>"
            html_str += (
                f'<object class="user-days-chart" style="" data="{svg_file_name}'
                f'" type="image/svg+xml"></object>'
            )

            html_str += "</div>"

    except Exception as error_msg:
        logger.opt(exception=error_msg).error(
            f"failed to generate hour line chart for git collaborator -> error: {error_msg}"
        )

html_str += (
    '<div style="width:98%;clear:both"><p>Last Modified: '
    + str(datetime.now(tzutc()).strftime("%d.%m.%Y %H:%M:%S (UTC)"))
    + "</p></div>"
)

html_str += "</div></body></html>"
with open(public_dir / "working_time.html", "w", encoding="utf-8") as fh:
    fh.write(html_str)
