import json
import logging
import re

# import sys
from datetime import datetime
from pathlib import Path

# from pprint import pprint

import htmlmin  # html minifier
import requests
from bs4 import BeautifulSoup
from pygments import highlight
from pygments.formatters import HtmlFormatter
from pygments.lexers import get_lexer_by_name

from log_helper import logger

working_dir = Path(__file__).parent / "public"

with open("weblate_contributors.json", "r", encoding="utf-8") as fh:
    all_translators = json.loads(str(fh.read()))

# pprint(all_translators)

headers = {"DNT": "1"}

weblate_api_url = "https://translate.codeberg.org/api/projects/tiny-weather-forecast-germany/languages/?format=json"
weblate_langs = requests.get(weblate_api_url, headers=headers, timeout=100).json()
# pprint(weblate_langs)

html_str = '<!DOCTYPE html><html lang="en"><head>'

html_str += """
<meta charset="utf-8">
<title>TinyWeatherForecastGermany - Weblate translators</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" sizes="180x180" href="../index/images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="../index/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="../index/images/favicon-16x16.png">
<link rel="manifest" href="../index/images/site.webmanifest">
<link rel="mask-icon" href="../index/images/safari-pinned-tab.svg" color="#293235">
<link rel="shortcut icon" href="../index/images/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Tiny Weather Forecast Germany">
<meta name="application-name" content="Tiny Weather Forecast Germany">
<meta name="msapplication-TileColor" content="#293235">
<meta name="msapplication-config" content="../index/images/browserconfig.xml">
<meta name="theme-color" content="#293235">
<meta name="robots" content="noindex, nofollow">
<meta name="thumbnail" content="../index/images/icon.png">
"""

html_str += '<style>:root {scrollbar-color: rgb(210,210,210) rgb(46,54,69) !important;scrollbar-width: thin !important;}body::-webkit-scrollbar {width:7px; height:15px}body::-webkit-scrollbar-track {background-color: rgb(210,210,210) !important;}body::-webkit-scrollbar-thumb {background-color:rgb(46,54,69) !important;height: 30px}a.black-link, a.black-link:active, a.black-link:focus, a.black-link:visited{color:black; text-decoration:none;} a.black-link:hover{text-decoration: underline;} progress {height: 9px; border-radius:3px;margin:2px;border-color:black;} #md-export-iframe,#json-export-iframe{border: 1px solid #292735;margin: 10px 0px;} @media (prefers-color-scheme: dark) {*, body {background-color:#000000!important; color: #ccc;}div, p, ul, li, strong, b, progress, code, pre, a.black-link{background-color:transparent!important; color: #CCCCCC!important;}#md-export-iframe,#json-export-iframe{border: 1px solid #292735;}}</style></head><body><div id="main-container" style="max-width: 80%; margin-right:auto; margin-left:auto;margin-top:20px;margin-bottom:20px;">'

html_str += (
    "<h1>TinyWeatherForecastGermany - Weblate translators</h1><p>Found "
    + str(len(all_translators.keys()))
    + " languages using <code>git log</code> via GitPython.</p><hr>"
)
markdown_str = "# TinyWeatherForecastGermany - Weblate translators\n\n"

for weblate_lang in weblate_langs:
    try:
        lang_name = str(weblate_lang["name"])
        lang_code = str(weblate_lang["code"])
        lang_percent = str(weblate_lang["translated_percent"])

        html_str += (
            '<div style="width:90%;padding:5px;margin:10px;">'
            '<span><a class="black-link" href="https://translate.codeberg.org/languages/'
            + lang_code
            + '/tiny-weather-forecast-germany/"><strong>'
            + lang_name
            + '</strong></a> <progress id="progress-'
            + str(re.sub(r"(?im)\W+", "", lang_name))
            + '" max="100" value="'
            + lang_percent
            + '"> '
            + lang_percent
            + "% </progress> "
            + lang_percent
            + "%</span>"
        )
        markdown_str += f"## {lang_name} ({lang_percent}%)\n\n"

        html_str += "<ul>\n"
        if lang_name in all_translators:
            for translator in all_translators[lang_name]:
                html_str += f"<li>{translator}</li>"
                markdown_str += f"* {translator}\n"
        else:
            error_msg = f"failed to parse translators for {lang_name} from"
            html_str += (
                f"<li><strong>ERROR:</strong> {error_msg} <code>git log</code></li>\n"
            )
            markdown_str += f"* **ERROR:** {error_msg} *git log* \n"
        html_str += "</ul></div>\n"
        markdown_str += "\n"
    except Exception as error_msg:
        html_str += (
            '<div style="width:90%;padding:5px;margin:10px;"><strong>ERROR:</strong> failed to process statistics for '
            f'{weblate_lang} <br> -> Error: <p>{error_msg}</p> </div>\n'
        )

lexer = get_lexer_by_name("markdown", stripall=True)
# logger.debug("lexer init completed")

# highlight lines by indices -> line numbers
hl_lines_i = []
formatter = HtmlFormatter(
    linenos=False,
    cssclass="sourcecode",
    full=True,
    style="perldoc",
    title=" ",
    lineanchors="mdexport",
    lineseparator="<br>",
    hl_lines=hl_lines_i,
    wrapcode=True,
)
# logger.debug("HtmlFormatter init completed")

md_highlight = highlight(markdown_str, lexer, formatter)
# logger.debug("highlight finished")

head_css = "\n:root {scrollbar-color: rgb(210,210,210) rgb(46,54,69) !important;scrollbar-width: thin !important;}body::-webkit-scrollbar {width:7px; height:15px}body::-webkit-scrollbar-track {background-color: rgb(210,210,210) !important;}body::-webkit-scrollbar-thumb {background-color:rgb(46,54,69) !important;height: 30px} body {background-color:#FFFFFF!important;} @media (prefers-color-scheme: dark) {body{filter:invert(1);background-color:rgb(22, 26, 34) !important;}}"

md_soup = BeautifulSoup(
    str(md_highlight), features="html.parser"
)  # parse html to modify elements
md_soup.select("h2")[0].decompose()
md_highlight = md_soup.select(".sourcecode")[0]
md_css = str(md_soup.select("head style")[0].text) + head_css

md_css = str(re.sub(r"(?m)\/\*[^\*]+\*\/", "", str(md_css)))
md_css = str(re.sub(r"(?m)[\n\r\t]+", "", str(md_css)))

md_soup.select("head style")[0].string = re.sub(r"<[^>]+>", "", str(md_css))

md_html_f = working_dir / "temp-md-export.html"
try:
    with open(md_html_f, "w+", encoding="utf-8") as fh:
        fh.write(
            htmlmin.minify(str(md_soup), remove_empty_space=True, remove_comments=True)
        )
except Exception as error_msg:
    logger.opt(exception=error_msg).error(f"minification of '{md_html_f}' failed -> error: {error_msg}")
    with open(md_html_f, "w+", encoding="utf-8") as fh:
        fh.write(str(md_soup))

html_str += "<div><details><summary><strong>Markdown export</strong></summary><br>"
html_str += '<a class="black-link" href="weblate.md">download</a><br>'
html_str += '<iframe id="md-export-iframe" style="width: 500px;max-width: 99%;min-height: 200px;" src="temp-md-export.html"></iframe>'
html_str += "</details></div><br>"

lexer = get_lexer_by_name("json", stripall=True)
# logger.debug("lexer init completed")

hl_lines_i = []
formatter = HtmlFormatter(
    linenos=False,
    cssclass="sourcecode",
    full=True,
    style="perldoc",
    title=" ",
    lineanchors="jsonexport",
    lineseparator="<br>",
    hl_lines=hl_lines_i,
    wrapcode=True,
)
# logger.debug("HtmlFormatter init completed")

json_highlight = highlight(str(json.dumps(all_translators, indent=4)), lexer, formatter)
# logger.debug("highlight finished")

j_soup = BeautifulSoup(
    str(json_highlight), features="html.parser"
)  # parse html to modify elements
j_soup.select("h2")[0].decompose()
json_highlight = j_soup.select(".sourcecode")[0]

json_css = str(j_soup.select("head style")[0].text) + head_css

json_css = str(re.sub(r"(?m)\/\*[^\*]+\*\/", "", str(json_css)))
json_css = str(re.sub(r"(?m)[\n\r\t]+", "", str(json_css)))

j_soup.select("head style")[0].string = str(json_css).strip("<style>").strip("</style>")

j_html_file = working_dir / "temp-json-export.html"
try:
    with open(j_html_file, "w+", encoding="utf-8") as fh:
        fh.write(
            htmlmin.minify(str(j_soup), remove_empty_space=True, remove_comments=True)
        )
except Exception as error_msg:
    logger.opt(exception=error_msg).error(f"minification of '{j_html_file}' failed -> error: {error_msg}")
    with open(j_html_file, "w+", encoding="utf-8") as fh:
        fh.write(str(j_soup))

html_str += "<div><details><summary><strong>JSON export</strong></summary><br>"
html_str += '<a class="black-link" href="weblate_contributors.json">download</a><br>'
html_str += '<iframe id="json-export-iframe" style="width: 500px;max-width: 99%;min-height: 200px;" src="temp-json-export.html"></iframe>'
html_str += "</details></div><br>"

html_str += (
    '<div style="width:98%;clear:both"><p>Last Modified at'
    + str(datetime.now().strftime("%Y-%m-%d on %H:%M:%S (UTC)"))
    + "</p></div>"
)

html_str += "</div></body></html>"
markdown_str = markdown_str.strip().strip("\n") + "\n"

with open("weblate.md", "w", encoding="utf-8") as fh:
    fh.write(markdown_str)

w_html_file = working_dir / "weblate.html"
try:
    with open(w_html_file, "w+", encoding="utf-8") as fh:
        fh.write(
            htmlmin.minify(str(html_str), remove_empty_space=True, remove_comments=True)
        )
except Exception as error_msg:
    logger.opt(exception=error_msg).error(f"minification of '{w_html_file}' failed -> error: {error_msg}")
    with open(w_html_file, "w+", encoding="utf-8") as fh:
        fh.write(str(html_str))
